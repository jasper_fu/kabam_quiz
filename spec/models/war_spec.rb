require 'spec_helper'

describe War do
  it { should belong_to :src_city }
  it { should belong_to :dest_city }
  it { should have_many :armies }

  let(:war) { create :war }

  before(:each) do
    city = war.src_city
    @resources = {
      :food   => city.food,
      :gold   => city.gold,
      :armies => city.armies,
      :wars   => city.attack_wars
    }
    @since = war.since.to_i
  end

  it "should have default value" do
    war.gold.should eq 0
    war.food.should eq 0
    war.since.should_not be_nil
    war.direction.should eq 'forth'
    war.distance.should > 0
  end

  context "update functions" do

    describe "move to target city" do
      it "will go forward to target" do
        expect { war.update_to(@resources, @since + 1) }.to change { war.distance }
      end

      it "will consume food as idle army" do
        expect { war.update_to(@resources, @since + 1) }.to change { @resources[:food].volume }
      end

      it "will update timestamp" do
        expect { war.update_to(@resources, @since + 1) }.to change { war.since.to_i }.by(1)
      end
    end

    describe "attack to target city" do
      before(:each) do
        war.distance = 0
      end

      it "will loss the army when attack" do
        expect { war.update_to(@resources, @since + 1) }.to change { war.armies.map(&:volume) }
      end

      it "will heist some resource" do
        expect { war.update_to(@resources, @since + 1) }.to change { [war.dest_city.food.volume, war.dest_city.gold.volume] }
      end
    end

    describe "retreat to city" do
      before(:each) do
        war.distance = 0
        war.direction = 'back'
        war.gold = 100
        war.food = 200
      end

      it "will turn in city with heisted food" do
        expect { war.update_to(@resources, @since + 1) }.to change { war.src_city.food.volume.round(0) }.by(200)
      end

      it "will turn in city with heisted gold" do
        expect { war.update_to(@resources, @since + 1) }.to change { war.src_city.gold.volume.round(0) }.by(100)
      end
    end
  end
end
