require 'time_forward_machine'

describe TimeForwardMachine do
  before(:each) do
    @now = Time.now

    population  = Resource.new(@now, 8)
    gold        = Resource.new(@now, 4)
    food        = Resource.new(@now, 2)
    tax         = Resource.new(@now, 1)
    armies      = [Resource.new(@now, 8), Resource.new(@now, 4)]
    attack_wars = [Resource.new(@now, 2), Resource.new(@now, 1)]

    @city = double('city')
    @city.stub(:population).and_return(population)
    @city.stub(:gold).and_return(gold)
    @city.stub(:food).and_return(food)
    @city.stub(:tax).and_return(tax)
    @city.stub(:armies).and_return(armies)
    @city.stub(:attack_wars).and_return(attack_wars)

    @tfm = TimeForwardMachine.new(@city)
  end

  it "should update all resources considering different frequence" do
    @tfm.forward_to(@now.to_i + 8)
    @city.population.updated_times.should eq 1
    @city.gold.updated_times.should eq 2
    @city.food.updated_times.should eq 4
    @city.tax.updated_times.should eq 8
    @city.armies[0].updated_times.should eq 1
    @city.armies[1].updated_times.should eq 2
    @city.attack_wars[0].updated_times.should eq 4
    @city.attack_wars[1].updated_times.should eq 8
  end

  class Resource
    attr_accessor :since
    attr_accessor :update_interval
    attr_accessor :save
    attr_accessor :updated_times

    def initialize(since, update_interval)
      @updated_times = 0
      @since = since
      @update_interval = update_interval
    end
    def update_to(resources, timestamp)
      @updated_times += 1
      @since = Time.at(timestamp)
    end
  end
end
