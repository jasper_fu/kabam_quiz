require 'spec_helper'

describe User do
  it { should have_many :cities }

  describe "scope" do
    let!(:user)        { create :user }
    let!(:new_city)    { create :new_city, :user => user }
    let!(:new_capital) { create :new_capital, :user => user }

    it "should be able to find capital city" do
      user.capital.should eq new_capital
    end
  end
end
