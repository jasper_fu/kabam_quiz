require 'spec_helper'

describe City do
  it { should belong_to :user }
  it { should validate_numericality_of :pos_x }
  it { should validate_numericality_of :pos_y }
  it { should validate_uniqueness_of   :pos_x }
  it { should validate_presence_of     :pos_x }
  it { should validate_presence_of     :pos_y }

  describe "set new capital" do
    let!(:user)        { create :user }
    let!(:new_city)    { create :new_city,    :user => user }
    let!(:new_capital) { create :new_capital, :user => user }

    it "should set new capital and cancel the old capital" do
      new_capital.is_capital.should be_true
      new_city.is_capital.should be_false
      new_city.set_capital
      new_city.is_capital.should be_true
      new_capital.reload.is_capital.should be_false
    end
  end

  describe "create a new city" do
    let!(:new_city) { create :new_city }

    it "should create all related resources" do
      new_city.gold.should_not be_nil
      new_city.food.should_not be_nil
      new_city.population.should_not be_nil
      new_city.tax.should_not be_nil
    end

    context "normal city half hour later" do
      before(:each) do
        since = new_city.gold.since.to_i
        Timecop.travel(Time.at(since+1800))
        new_city.update_to_now
      end

      it "should update all resources" do
        new_city.population.volume.should eq PM_CONFIG[:population][:init_value]
        new_city.gold.volume.round(0).should eq PM_CONFIG[:gold][:init_value] + PM_CONFIG[:gold][:city] * 1800.0 / PM_CONFIG[:gold][:time_cost]
        new_city.food.volume.round(0).should eq PM_CONFIG[:food][:init_value] + PM_CONFIG[:food][:city] * 1800.0 / PM_CONFIG[:food][:time_cost]
      end
    end

    context "1 hours later" do
      before(:each) do
        since = new_city.gold.since.to_i
        Timecop.travel(Time.at(since+3600))
        new_city.update_to_now
      end
      it "should update all resources" do
        new_city.population.volume.should eq (PM_CONFIG[:population][:init_value] * 1.05 ).round(0)
        gold = PM_CONFIG[:gold][:init_value] + PM_CONFIG[:gold][:city] * 3600.0 / PM_CONFIG[:gold][:time_cost]
        gold *= 1 - PM_CONFIG[:tax][:init_value]
        new_city.gold.volume.round(0).should eq gold.round(0)
      end
    end
  end

  describe "describe war" do
    let(:src_city)  { create :new_city }
    let(:dest_city) { create :new_city }

    before(:each) do
      src_city.armies.each { |army| army.update_attributes :volume => 1000 }
    end

    it "should create war army as params" do
      src_city.declare_war(dest_city, { :spear => 100, :archer => 200 })
      src_city.armies[0].reload.volume.should eq 900
      src_city.armies[1].reload.volume.should eq 800
      src_city.armies.count.should eq 5
      src_city.attack_wars.count.should eq 1
      dest_city.defence_wars.count.should eq 1
    end
  end
end
