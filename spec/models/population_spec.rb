require 'spec_helper'

describe Population do
  it { should belong_to :city }

  let(:population) { Population.create }

  it "should have default value" do
    population.volume.should eq PM_CONFIG[:population][:init_value]
    population.since.should_not be_nil
  end
end
