require 'spec_helper'

describe Army do

  let(:city)   { create :new_city }

  it "should have default value" do
    archer = Army.new
    archer.volume.should eq 0
    archer.quarter.should eq "wait_training"
    archer.since.should_not be_nil
  end

  context "validation" do
    it "should validate gold when create" do
      army = Army.new({:volume => 10, :service => "spear", :city => city})
      army.save.should be_false
    end

    it "should validate population when create" do
      city.gold.volume = 300
      city.gold.save
      city.population.volume = 5
      city.population.save
      army = Army.new({:volume => 10, :service => "spear", :city => city})
      army.save.should be_false
    end

    it "should validate training queue size when create" do
      city.gold.volume = 300
      city.gold.save
      city.population.volume = 50000
      city.population.save
      5.times do
        army = Army.new({:volume => 10, :service => "spear", :city => city})
        army.save.should be_true
      end
      army = Army.new({:volume => 10, :service => "spear", :city => city})
      army.save.should be_false
    end
  end

  it "should charge gold after training a new army" do
    city.gold.volume = 300
    city.gold.save
    city.population.volume = 50000
    city.population.save

    5.times do
      army = Army.new({:volume => 100, :service => "spear", :city => city})
      army.save.should be_true
    end
    city.gold.volume.should eq (300 - PM_CONFIG[:army][:training][:spear][:gold] * 5)
    city.population.volume.should eq (50000 - 100 * 5)
  end

  context "training a new army" do
    before(:each) do
      city.gold.volume = 300
      city.gold.save
      city.population.volume = 30000
      city.population.save
    end

    it "should create a training army if no stale army exist" do
      army = Army.train(city, {:service => 'spear', :volume => 100})
      army.should be_valid
    end

    it "will reuse stale army" do
      stale_army = city.armies.create!(:service => "spear", :quarter => "stale", :volume => 100)
      army = Army.train(city, {:service => 'archer', :volume => 400})
      army.id.should eq stale_army.id
      army.volume.should eq 400
      army.service.should eq 'archer'
    end
  end

  context "update to latest state" do

    let(:city) { create :new_city }

    before(:each) do
      city.population.volume = 10000
      city.food.volume       = 10000
      city.gold.volume       = 1000
      city.population.save
      city.food.save
      city.gold.save
      city.armies[0].volume  = 3600
      @army_0  = city.armies[0]
      @army_1  = city.armies.create(:service => 'archer',  :quarter => "training",       :volume => 10)
      @army_2  = city.armies.create(:service => 'cavalry', :quarter => "wait_training", :volume => 20)
      @since_0 = @army_0.since.to_i + 1
      @since_1 = @army_1.since.to_i + 1
      @since_2 = @army_2.since.to_i + 1
      @resources = { :food => city.food, :armies => city.armies }
    end

    it "should charge food for idle army" do
      @army_0.update_to @resources, @since_0
      city.food.volume.round(0).should eq (10000 - PM_CONFIG[:army][:idle][:spear][:food])
      @army_0.since.to_i.should eq @since_0
    end

    it "should update training army if it's done" do
      @army_1.update_to @resources, @since_1
      @army_1.quarter.should eq 'stale'
      @resources[:armies][1].volume.should eq 10
      @army_1.since.to_i.should eq @since_1
    end

    it "should update idle army if it's done" do
      @resources[:armies].reject! { |army| army.id == @army_1.id }
      @army_2.update_to @resources, @since_2
      @army_2.quarter.should eq 'training'
      @army_2.since.to_i.should eq @since_2
    end
  end
end
