require 'spec_helper'

describe Food do
  it { should belong_to :city }

  let(:food) { Food.create }

  it "should have default value" do
    food.volume.should eq PM_CONFIG[:food][:init_value]
    food.since.should_not be_nil
  end

  describe "update to latest" do
    context "capital" do
      let(:city)   { create :new_capital }
      let!(:since) { city.food.since.to_i }

      it "should update to correct volume" do
        city.food.update_to( {}, since + 10 )
        city.food.volume.should eq 10 * Rational(PM_CONFIG[:food][:capital], PM_CONFIG[:food][:time_cost])
        city.food.since.should eq Time.at(since + 10)
      end
    end

    context "normal city" do
      let(:city)   { create :new_city }
      let!(:since) { city.food.since.to_i }

      it "should update to correct volume" do
        city.food.update_to( {}, since + 10 )
        city.food.volume.should eq 10 * Rational(PM_CONFIG[:food][:city], PM_CONFIG[:food][:time_cost])
        city.food.since.should eq Time.at(since + 10)
      end
    end
  end
end
