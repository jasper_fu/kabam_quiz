require 'spec_helper'

describe Gold do
  it { should belong_to :city }

  let(:gold)  { Gold.create }

  it "should have default value" do
    gold.volume.should eq PM_CONFIG[:gold][:init_value]
    gold.since.should_not be_nil
  end

  describe "update to latest" do
    context "capital" do
      let(:city)   { create :new_capital }
      let!(:since) { city.gold.since.to_i }

      it "should update to correct volume" do
        city.gold.update_to( {}, since + 10 )
        city.gold.volume.should eq 10 * Rational(PM_CONFIG[:gold][:capital], PM_CONFIG[:gold][:time_cost])
        city.gold.since.should eq Time.at(since + 10)
      end
    end

    context "normal city" do
      let(:city)   { create :new_city }
      let!(:since) { city.gold.since.to_i }

      it "should update to correct volume" do
        city.gold.update_to( {}, since + 10 )
        city.gold.volume.should eq 10 * Rational(PM_CONFIG[:gold][:city], PM_CONFIG[:gold][:time_cost])
        city.gold.since.should eq Time.at(since + 10)
      end
    end
  end
end
