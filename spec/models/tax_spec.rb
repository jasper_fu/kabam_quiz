require 'spec_helper'

describe Tax do
  it { should belong_to :city }

  let(:tax)  { Tax.create }

  it "should have default value" do
    tax.volume.should eq PM_CONFIG[:tax][:init_value]
    tax.since.should_not be_nil
  end

  describe "update to latest" do
    let(:city)      { create :new_city }
    let(:resources) { { :population => city.population, :gold => city.gold, :food => city.food, :armies => city.armies } }
    let!(:since)    { city.tax.since.to_i }

    it "should not update in short period" do
      expect { city.tax.update_to(resources, since + 1) }.to_not change { city.tax.since }
    end

    it "should update 'since' to a predicted time" do
      expect { city.tax.update_to(resources, since + 10000) }.to change { city.tax.since.to_i }.by((10000/city.tax.update_interval) * city.tax.update_interval)
    end

    it "should decrease army if food is 0" do
      resources[:armies][0].volume = 100
      expect { city.tax.update_to(resources, since + 3600) }.to change { resources[:armies][0].volume }.by( -100 * PM_CONFIG[:army][:famine_dead_rate] )
    end

    context "relative higher tax rate" do
      before(:each) do
        city.population.volume = 100
        city.gold.volume       = 1000
        city.food.volume       = 1000
        city.tax.volume        = 0.9
        @resources = { :population => city.population, :gold => city.gold, :food => city.food }
      end

      it "should decrease gold and increase population by a given rate" do
        city.tax.update_to(@resources, since + city.tax.update_interval)
        city.population.volume.should eq 100 * ( 1 + PM_CONFIG[:tax][:produce][:population][:rate])
        city.gold.volume.should eq 1000 * ( 1 - city.tax.volume )
      end

      it "should increase population 1000 at maximum" do
        city.population.volume = 100000
        city.tax.volume        = 900
        city.tax.update_to(@resources, since + city.tax.update_interval)
        city.population.volume.should eq 101000
        city.gold.volume.should eq 0
      end
    end

    context "relative lower tax rate" do
      before(:each) do
        city.population.volume = 1000
        city.gold.volume       = 1000
        city.food.volume       = 1000
        city.tax.volume        = 0.9
        @resources = { :population => city.population, :gold => city.gold, :food => city.food }
      end

      it "should decrease gold and increase population by a given rate" do
        city.tax.update_to(@resources, since + city.tax.update_interval)
        city.population.volume.should eq 1000 * ( 1 - PM_CONFIG[:tax][:produce][:population][:rate])
        city.gold.volume.should eq 1000 * ( 1 - city.tax.volume )
      end

      it "should decrease population 1000 at maximum" do
        city.population.volume = 100000
        city.tax.volume        = 0.9
        city.tax.update_to(@resources, since + city.tax.update_interval)
        city.population.volume.should eq 99000
      end
    end
  end
end
