FactoryGirl.define do
  factory :war do
    after(:build) do |war|
      war.src_city  = create :glory_city
      war.dest_city = create :glory_city
      war.armies << war.src_city.armies.create(:volume => 100, :service => "spear", :quarter => "warlike")
    end
  end
end
