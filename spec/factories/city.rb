FactoryGirl.define do
  factory :new_city, class: City do
    pos_x { rand(99) }
    pos_y { rand(99) }
    is_capital false
  end

  factory :new_capital, class: City do
    pos_x { rand(99) }
    pos_y { rand(99) }
    is_capital true
  end

  factory :glory_city, class: City do
    pos_x { rand(99) }
    pos_y { rand(99) }
    is_capital false

    after(:create) do |war|
      war.gold.update_attributes :volume => 100000
      war.food.update_attributes :volume => 100000
      war.population.update_attributes :volume => 100000
      war.armies.each { |a| a.update_attributes :volume => 3000 }
    end
  end
end
