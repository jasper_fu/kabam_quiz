FactoryGirl.define do
  factory :spear, class: Army do
    service "spear"
    volumn 10
  end

  factory :archer, class: Army do
    service "archer"
    volumn 30
  end

  factory :cavalry, class: Army do
    service "cavalry"
    volumn 30
  end
end
