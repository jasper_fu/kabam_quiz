class TimeForwardMachine
  attr_accessor :resources

  def initialize(city)
    @resources = {}
    add_all_resources city
  end

  def add_all_resources(city)
    @resources[:population]  = city.population
    @resources[:gold]        = city.gold
    @resources[:armies]      = city.armies
    @resources[:attack_wars] = city.attack_wars
    @resources[:food]        = city.food
    @resources[:tax]         = city.tax
  end

  # Update all related resources to current state
  def forward_to(now)
    oldest_timestamp = @resources.values.flatten.map(&:since).min.to_i
    (oldest_timestamp..now).each do |timestamp|
      @resources.values.flatten.each do |resource|
        next if resource.since.to_i >= timestamp
        resource.update_to(@resources, timestamp) if should_update(resource, timestamp)
      end
    end
    @resources.values.flatten.each { |resource| resource.save }
  end

  private
  def should_update(resource, timestamp)
    return false if resource.update_interval <= 0
    (timestamp - resource.since.to_i) / resource.update_interval > 0
  end
end
