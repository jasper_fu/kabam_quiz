Feature: City
  In order start a game
  As a player
  I want to be able to create ande view a city

  Scenario: Create a new City
    Given I am a valid user
    When I create a new city with given position 14, 20
    Then I should have a new city

  Scenario: Set tax rate for city
    Given I am a valid user and own a city
    When I change tax rate from 0.2 to 0.4 for first city
    Then the city should have 0.4 tax rate

  Scenario: Setup a capital
    Given I am a valid user and own 2 normal cities and capital
    When I set first normal city to capital
    Then the normal city should became capital now
    And the original capital should be a normal city

  Scenario Outline: City Growth
    Given I am a valid user and own a city
    When the gold is "<gold>"
    And the food is "<food>"
    And the population is "<population>"
    And the tax is "<tax>"
    And "<time>" seconds later
    And I query city state
    Then the gold should be "<gold_now>"
    Then the food should be "<food_now>"
    Then the population should be "<population_now>"

  Scenarios: Read City Info
    | gold | food | population | tax | time | gold_now | food_now | population_now |
    | 1000 | 1000 | 1000       | 0.2 | 1800 | 1500     | 1500     | 1000           |
    | 1000 | 1000 | 1000       | 0.2 | 3600 | 1600     | 2000     | 950            |
    | 1000 | 1000 | 100        | 0.2 | 3600 | 1600     | 2000     | 105            |
