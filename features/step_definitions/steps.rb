Given(/^I am a valid user$/) do
  @user = User.create(:name => Faker::Name.name)
end

When(/^I create a new city with given position (\d+), (\d+)$/) do |pos_x, pos_y|
  message = post user_cities_path(@user, {:city => { :pos_x => pos_x, :pos_y => pos_y }})
  @city = JSON.parse(message.body)
end

Then(/^I should have a new city$/) do
  @city["type"].should eq 'msg'
  @city["message"]["id"].should_not be_nil
  @user.cities.should have(1).city
end

Given(/^I am a valid user and own (\d+) normal cities and capital$/) do |cities|
  @user = User.create(:name => Faker::Name.name)
  cities.to_i.times { post user_cities_path(@user, {:city => { :pos_x => rand(99), :pos_y => rand(99) }}) }
  post user_cities_path(@user, {:city => { :pos_x => rand(99), :pos_y => rand(99), :is_capital => true }})

  @normal_city = @user.reload.cities.first
  @capital     = @user.cities.capital.first
end

When(/^I set first normal city to capital$/) do
  put set_capital_user_city_path(@user, @normal_city)
end

Then(/^the normal city should became capital now$/) do
  @normal_city.reload.is_capital.should be_true
end

Then(/^the original capital should be a normal city$/) do
  @capital.reload.is_capital.should be_false
end

Given(/^I am a valid user and own a city$/) do
  @user = User.create(:name => Faker::Name.name)
  post user_cities_path(@user, {:city => { :pos_x => rand(99), :pos_y => rand(99) }})
  @city = @user.reload.cities.first
end

When(/^I change tax rate from (\d+\.\d+) to (\d+.\d+) for first city$/) do |origin_rate, update_rate|
  put user_city_path(@user, @city, { :tax_rate => update_rate.to_f })
end

Then(/^the city should have (\d+\.\d+) tax rate$/) do |new_tax_rate|
  @city.reload.tax.volume.should eq new_tax_rate.to_f
end

When(/^the gold is "(.*?)"$/) do |volume|
  @city.gold.update_attributes :volume => volume
end

When(/^the food is "(.*?)"$/) do |volume|
  @city.food.update_attributes :volume => volume
end

When(/^the population is "(.*?)"$/) do |volume|
  @city.population.update_attributes :volume => volume
end

When(/^the tax is "(.*?)"$/) do |volume|
  @city.tax.update_attributes :volume => volume
end

When(/^"(.*?)" seconds later$/) do |seconds|
  Timecop.travel(Time.at(@start_time.to_i + seconds.to_i))
end

When(/^I query city state$/) do
  message = get user_city_path(@user, @city)
end

Then(/^the gold should be "(.*?)"$/) do |volume|
  @city.gold.reload.volume.round(0).should eq volume.to_i
end

Then(/^the food should be "(.*?)"$/) do |volume|
  @city.food.reload.volume.round(0).should eq volume.to_i
end

Then(/^the population should be "(.*?)"$/) do |volume|
  @city.population.reload.volume.round(0).should eq volume.to_i
end
