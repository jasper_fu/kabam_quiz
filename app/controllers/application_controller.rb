class ApplicationController < ActionController::Base
  protect_from_forgery

  def render_error(error_code)
    render :json => { :type => "error", :error_code => error_code }
  end

  def render_message(message)
    render :json => { :type => "msg", :message => message }
  end
end
