class WarsController < ApplicationController

  before_filter :get_and_update_city

  def create
    @war = War.new(params[:war])
    if @war.save
      render_message @war
    else
      render_error @war.errors
    end
  end

  protected
  def get_and_update_city
    @city = City.find(params[:city_id])
    @city.update_to_now
  end

end
