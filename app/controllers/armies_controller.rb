class ArmiesController < ApplicationController

  before_filter :get_and_update_city

  def index
    render_message @city.armies
  end

  # This will training a new troop
  # @param [Hash] the { :army => { :type => 'spear/archer/cavalry', :volume => 100 } }
  def create
    if Army.train(@city, params[:army])
      render_message @army
    else
      render_error @army.errors
    end
  end

  def destroy
    @army = @city.armies.find(params[:id])
    if @army.update_attributes({:quarter => 'stale'})
      render_message @army
    else
      render_error "failed"
    end
  end

  protected
  def get_and_update_city
    @city = City.find(params[:city_id])
    @city.update_to_now
  end
end
