class CitiesController < ApplicationController

  before_filter :get_user
  before_filter :update_city, :except => [:create]

  def show
    render_message({
      :tax_rate   => @city.tax.volume,
      :gold       => @city.gold.volume.to_i,
      :population => @city.population.volume.to_i,
      :food       => @city.food.volume.to_i
    })
  end

  # We can update tax_rate here
  # @param params [Integer] the city ID
  # @param params [Float] the new tax rate of the city
  def update
    if @city.tax.update_attributes(:volume => params[:tax_rate])
      render_message @city
    else
      render_error @city.errors
    end
  end

  # Create a new city with given position
  # @param params [Hash] the city ID, {:city => {:pos_x => 0, :pos_y => 1}}
  def create
    @city = @user.cities.new(params[:city])

    if @city.save
      render_message @city
    else
      render_error @city.errors
    end
  end

  def set_capital
    @city.set_capital

    if @city.is_capital?
      render_error ERROR_CODE[:city][:already_capital]
    else
      render_message @city
    end
  end

  protected
  def update_city
    @city = @user.cities.find(params[:id])
    @city.update_to_now
  end

  def get_user
    @user = User.find(params[:user_id])
  end
end
