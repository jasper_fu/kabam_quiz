class War < ActiveRecord::Base
  attr_accessible :dest_city, :since, :src_city, :direction, :distance, :gold, :food

  belongs_to :src_city,  :foreign_key => :src_city_id,  :class_name => 'City'
  belongs_to :dest_city, :foreign_key => :dest_city_id, :class_name => 'City'

  has_many :armies

  after_initialize :set_default_value
  after_save :set_distance

  def set_default_value
    self.gold      ||= 0
    self.food      ||= 0
    self.since     ||= Time.now
    self.direction ||= 'forth'
  end

  def update_interval
    return -1 if is_stale?
    1
  end

  def update_to(resources, timestamp)
    r_armies = resources[:armies].select { |army| army.is_warlike? and army.war_id == self.id }
    # it will consume food as idle army
    r_armies.each { |army| army.update_idle_army(resources, timestamp) }

    self.distance = distance - velocity

    if distance < 0
      attack(r_armies, timestamp)                if is_forth?
      enter_city(resources, r_armies, timestamp) if is_back?
    end
    self.since = Time.at(timestamp)
  end

  def attack(armies, timestamp)
    dest_city.update_to_now(timestamp)
    self.war_result(armies)
    if armies.inject(0) { |result, army| result += army.volume } > 0
      self.distance  = total_distance
      self.direction = 'back'
    else
      armies.each { |army| army.quarter = 'stale' }
      self.direction = 'stale'
    end
  end

  def enter_city(resources, armies, timestamp)
    armies.each { |army| army.quarter = 'stale' }
    city_food = resources[:food]
    city_gold = resources[:gold]
    city_food.volume += food
    city_gold.volume += gold
  end

  def is_stale?
    direction == 'stale'
  end

  def is_forth?
    direction == 'forth'
  end

  def is_back?
    direction == 'back'
  end

  def war_result(armies)
    armies.each { |army| army.volume *= rand }
    gold_heisted = dest_city.gold.volume * rand
    food_heisted = dest_city.food.volume * rand

    dest_city.gold.volume -= gold_heisted
    dest_city.food.volume -= food_heisted

    dest_city.gold.save
    dest_city.food.save

    dest_city.armies.idle.each do |army|
      army.volume *= rand
      army.save
    end

    self.gold = gold_heisted
    self.food = food_heisted
  end

  private

  def total_distance
    Math.sqrt((src_city.pos_x - dest_city.pos_x) ** 2 + (src_city.pos_y - dest_city.pos_y) ** 2)
  end

  def velocity
    armies.map { |army| PM_CONFIG[:army][:warlike][army.service][:velocity] }.min
  end

  def set_distance
    update_attribute(:distance, total_distance) if src_city and dest_city and !distance
  end
end
