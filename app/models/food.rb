class Food < ActiveRecord::Base
  belongs_to :city
  attr_accessible :since, :volume

  after_initialize :set_default_value

  def set_default_value
    self.volume ||= PM_CONFIG[:food][:init_value]
    self.since  ||= Time.now
  end

  def update_interval
    PM_CONFIG[:food][:update_interval]
  end

  def update_to(resources, timestamp)
    if city.is_capital
      incr = Rational(PM_CONFIG[:food][:capital], PM_CONFIG[:food][:time_cost])
    else
      incr = Rational(PM_CONFIG[:food][:city],    PM_CONFIG[:food][:time_cost])
    end

    self.volume += incr * (timestamp - since.to_i)
    self.since   = Time.at(timestamp)
  end
end
