class Army < ActiveRecord::Base
  belongs_to :city
  belongs_to :war
  attr_accessible :volume, :quarter, :since, :service, :city, :war

  after_initialize :set_default_value

  scope :training_queue, where(:quarter => ['training', 'wait_training'])
  scope :idle,           where(:quarter => 'idle')
  scope :stale,          where(:quarter => 'stale')

  validates_inclusion_of :service, :in => %w(spear archer cavalry)

  validate :check_training_queue_size, :on => :create
  validate :check_population_size,     :on => :create
  validate :check_gold,                :on => :create

  after_save :charge_resources

  def set_default_value
    self.quarter ||= "wait_training"
    self.volume  ||= 0
    self.since   ||= Time.now
  end

  def update_interval
    return -1 if quarter == 'stale'
    PM_CONFIG[:army][quarter][service][:update_interval]
  end

  def update_to(resources, timestamp)
    update_training_army(resources, timestamp) if in_training_queue?
    update_idle_army(resources, timestamp)     if is_idle?
    self.since = Time.at timestamp
  end

  # Train a new army, it will reuse the stale army record if possible.
  def self.train(city, params)
    stale_army = city.armies.stale.first
    if stale_army
      params[:since]   = Time.now
      params[:quarter] = 'wait_training'
      stale_army.update_attributes params
      stale_army
    else
      city.armies.create params
    end
  end

  #Update functions
  def update_training_army(resources, timestamp)
    if is_training?
      army = resources[:armies].select { |a| a.service == service }.first
      army.volume += volume
      self.quarter = "stale"
    else
      unless resources[:armies].any? { |a| a.is_training? }
        self.quarter = "training"
      end
    end
  end

  def update_idle_army(resources, timestamp)
    return unless resources[:food].volume != 0
    food_consume = volume.to_f * PM_CONFIG[:army][:idle][service][:food] / PM_CONFIG[:army][:idle][:time_cost]
    total_food = resources[:food].volume
    total_food -= food_consume
    resources[:food].volume = total_food < 0 ? 0 : total_food
  end

  private
  # validations
  def check_training_queue_size
    return true unless in_training_queue?
    if city.armies.training_queue.length >= PM_CONFIG[:army][:training][:max_queue_size]
      errors.add(:city_id, "can't exceed maximium training size")
    end
  end

  def check_population_size
    return true unless in_training_queue?
    if city.population.volume < volume
      errors.add(:city_id, "no enough population to recruit")
    end
  end

  def check_gold
    return true unless in_training_queue?
    if city.gold.volume < PM_CONFIG[:army][:training][service][:gold]
      errors.add(:city_id, "no enough gold to recruit")
    end
  end

  def charge_resources
    return true unless in_training_queue?
    city.gold.volume -= PM_CONFIG[:army][:training][service][:gold]
    city.gold.save
    city.population.volume -= volume
    city.population.save
  end

  public
  # States query
  def is_training?
    quarter == "training"
  end

  def is_wait_training?
    quarter == "wait_training"
  end

  def in_training_queue?
    quarter == "training" or quarter == "wait_training"
  end

  def is_idle?
    quarter == "idle"
  end

  def is_warlike?
    quarter == "warlike"
  end
end
