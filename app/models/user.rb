class User < ActiveRecord::Base
  attr_accessible :name
  has_many :cities

  def capital
    cities.capital.first
  end
end
