class Population < ActiveRecord::Base
  belongs_to :city
  attr_accessible :since, :volume

  after_initialize :set_default_value

  def set_default_value
    self.volume ||= PM_CONFIG[:population][:init_value]
    self.since  ||= Time.now
  end

  def update_interval
    3600 * 365
  end

  def update_to(resources, timestamp)

  end
end
