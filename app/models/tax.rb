class Tax < ActiveRecord::Base
  belongs_to :city
  attr_accessible :city, :since, :volume

  after_initialize :set_default_value

  def set_default_value
    self.volume ||= PM_CONFIG[:tax][:init_value]
    self.since  ||= Time.now
  end

  def update_interval
    PM_CONFIG[:tax][:update_interval]
  end

  def update_to(resources, timestamp)
    round = (timestamp - since.to_i) / update_interval
    return unless round > 0

    update_population_and_gold(resources, timestamp)
    update_army(resources, timestamp)

    self.since = Time.at(since.to_i + round * update_interval)
  end

  def update_army(resources, timestamp)
    return unless resources[:food].volume == 0
    resources[:armies].select {|army| army.quarter == 'idle' }.each do |army|
      army.volume *= 1 - PM_CONFIG[:army][:famine_dead_rate]
    end
  end

  # update gold, food and population
  # if population greater than tax_rate * 1000, it will increase population by 5%
  # otherwise it will decrease 5%
  # the maximum volume change of population is 1000.
  def update_population_and_gold(resources, timestamp)
    if resources[:population].volume < volume * PM_CONFIG[:tax][:sensibility]
      rate = PM_CONFIG[:tax][:consume][:population][:rate]
      var  = (resources[:population].volume * rate).to_i
      max  = PM_CONFIG[:tax][:consume][:population][:max]
      min  = PM_CONFIG[:tax][:consume][:population][:min]
      if var > max
        var = max
      elsif var < min
        var = min
      end
      resources[:population].volume += var
    else
      rate = PM_CONFIG[:tax][:produce][:population][:rate]
      var  = (resources[:population].volume * rate).to_i
      max  = PM_CONFIG[:tax][:produce][:population][:max]
      min  = PM_CONFIG[:tax][:produce][:population][:min]
      if var > max
        var = max
      elsif var < min
        var = min
      end
      resources[:population].volume -= var
    end

    resources[:gold].volume *= 1 - volume

    # negative value defence
    resources[:gold].volume       = resources[:gold].volume       < 0 ? 0 : resources[:gold].volume
    resources[:population].volume = resources[:population].volume < 0 ? 0 : resources[:population].volume
  end
end
