class City < ActiveRecord::Base
  belongs_to :user
  attr_accessible :is_capital, :pos_x, :pos_y, :since, :tax_rate

  has_one  :population
  has_one  :gold
  has_one  :food
  has_one  :tax
  has_many :armies
  has_many :attack_wars,  :foreign_key => :src_city_id,  :class_name => 'War'
  has_many :defence_wars, :foreign_key => :dest_city_id, :class_name => 'War'

  scope :is_occupied, lambda { |pos_x, pos_y| where(:pos_x => pos_x, :pos_y => pos_y).first }
  scope :capital,     where(:is_capital => true)

  validates_numericality_of :pos_x, :pos_y,
    :only_integer             => true,
    :greater_than_or_equal_to => PM_CONFIG[:position][:min],
    :less_than_or_equal_to    => PM_CONFIG[:position][:max]
  validates_uniqueness_of   :pos_x, :scope => :pos_y
  validates_presence_of     :pos_x, :pos_y

  after_create :setup_resources

  # Set current city as capital and cancel the origin one.
  def set_capital
    return if is_capital?
    user.capital.update_attribute(:is_capital, false) if user.capital
    self.update_attribute(:is_capital, true)
  end

  def setup_resources
    self.create_population
    self.create_food
    self.create_gold
    self.create_tax
    %w(spear archer cavalry).each do |service|
      self.armies.create(:service => service, :volume => 0, :quarter => 'idle')
    end
  end

  def update_to_now(timestamp=Time.now.to_i)
    TimeForwardMachine.new(self).forward_to(timestamp)
  end

  def declare_war(city, send_armies)
    send_armies.each do |service, volume|
      army = armies.idle.select { |a| a.service == service.to_s }.first
      return false if army.volume < volume
    end

    war = attack_wars.create(:dest_city => city)

    send_armies.each do |service, volume|
      army = armies.idle.select { |a| a.service == service.to_s }.first
      army.volume -= volume
      army.save
      armies.create({
        :war     => war,
        :volume  => volume,
        :quarter => "warlike",
        :service => service.to_s
      })
    end
  end

end
