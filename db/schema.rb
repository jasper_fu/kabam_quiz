# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130428035151) do

  create_table "armies", :force => true do |t|
    t.string   "service"
    t.integer  "volume"
    t.datetime "since"
    t.integer  "city_id"
    t.string   "quarter"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "war_id"
  end

  add_index "armies", ["city_id"], :name => "index_armies_on_city_id"
  add_index "armies", ["war_id"], :name => "index_armies_on_war_id"

  create_table "cities", :force => true do |t|
    t.integer  "user_id"
    t.integer  "pos_x"
    t.integer  "pos_y"
    t.boolean  "is_capital"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "cities", ["user_id"], :name => "index_cities_on_user_id"

  create_table "foods", :force => true do |t|
    t.float    "volume"
    t.integer  "city_id"
    t.datetime "since"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "foods", ["city_id"], :name => "index_foods_on_city_id"

  create_table "golds", :force => true do |t|
    t.float    "volume"
    t.integer  "city_id"
    t.datetime "since"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "golds", ["city_id"], :name => "index_golds_on_city_id"

  create_table "populations", :force => true do |t|
    t.integer  "volume"
    t.integer  "city_id"
    t.datetime "since"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "populations", ["city_id"], :name => "index_populations_on_city_id"

  create_table "taxes", :force => true do |t|
    t.integer  "city_id"
    t.float    "volume"
    t.datetime "since"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "wars", :force => true do |t|
    t.integer  "src_city_id"
    t.integer  "dest_city_id"
    t.datetime "since"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "direction"
    t.float    "distance"
    t.float    "gold"
    t.float    "food"
  end

  add_index "wars", ["dest_city_id"], :name => "index_wars_on_dest_city_id"
  add_index "wars", ["src_city_id"], :name => "index_wars_on_src_city_id"

end
