class AddGoldAndFoodToWar < ActiveRecord::Migration
  def change
    add_column :wars, :gold, :float
    add_column :wars, :food, :float
  end
end
