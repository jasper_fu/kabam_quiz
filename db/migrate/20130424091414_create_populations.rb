class CreatePopulations < ActiveRecord::Migration
  def change
    create_table :populations do |t|
      t.integer :volume
      t.references :city
      t.timestamp :since

      t.timestamps
    end
    add_index :populations, :city_id
  end
end
