class CreateGolds < ActiveRecord::Migration
  def change
    create_table :golds do |t|
      t.integer :volume
      t.references :city
      t.timestamp :since

      t.timestamps
    end
    add_index :golds, :city_id
  end
end
