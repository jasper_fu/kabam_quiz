class CreateTaxes < ActiveRecord::Migration
  def change
    create_table :taxes do |t|
      t.references :city
      t.float :volume
      t.timestamp :since

      t.timestamps
    end
  end
end
