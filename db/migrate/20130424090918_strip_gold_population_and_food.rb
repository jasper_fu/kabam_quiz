class StripGoldPopulationAndFood < ActiveRecord::Migration
  def change
    remove_column :cities, :gold
    remove_column :cities, :population
    remove_column :cities, :food
    remove_column :cities, :tax_rate
    remove_column :cities, :since
  end
end
