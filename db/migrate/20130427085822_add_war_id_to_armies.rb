class AddWarIdToArmies < ActiveRecord::Migration
  def change
    add_column :armies, :war_id, :integer
    add_index  :armies, :war_id
  end
end
