class ChangeVolumnToFloatType < ActiveRecord::Migration
  def change
    change_column :golds, :volume, :float
    change_column :foods, :volume, :float
  end
end
