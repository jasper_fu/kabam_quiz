class CreateWars < ActiveRecord::Migration
  def change
    create_table :wars do |t|
      t.integer :src_city_id
      t.integer :dest_city_id
      t.timestamp :since

      t.timestamps
    end

    add_index :wars, :src_city_id
    add_index :wars, :dest_city_id
  end
end
