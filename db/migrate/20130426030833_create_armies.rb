class CreateArmies < ActiveRecord::Migration
  def change
    create_table :armies do |t|
      t.string :service
      t.integer :volume
      t.timestamp :since
      t.references :city
      t.string :quarter

      t.timestamps
    end
    add_index :armies, :city_id
  end
end
