class AddDirectionAndDistanceToWar < ActiveRecord::Migration
  def change
    add_column :wars, :direction, :string
    add_column :wars, :distance, :float
  end
end
