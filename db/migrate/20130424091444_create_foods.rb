class CreateFoods < ActiveRecord::Migration
  def change
    create_table :foods do |t|
      t.integer :volume
      t.references :city
      t.timestamp :since

      t.timestamps
    end
    add_index :foods, :city_id
  end
end
