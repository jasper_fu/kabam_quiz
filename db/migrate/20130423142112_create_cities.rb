class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.references :user
      t.integer :pos_x
      t.integer :pos_y
      t.integer :gold
      t.integer :food
      t.integer :population
      t.float :tax_rate
      t.boolean :is_capital
      t.timestamp :since

      t.timestamps
    end
    add_index :cities, :user_id
  end
end
